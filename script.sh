#!/bin/bash

for file in "$@"; do
        for method in 'ordered' 'fstein' 'byline' 'threshold' 'rand'; do
                python main.py --image $file --method $method
        done
done
