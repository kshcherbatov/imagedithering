import numpy as np
import random as rand
from PIL import Image
import argparse
import sys, os

class Dithering:
        @classmethod
        def __prepare(cls, image, make_copy = True):
                width, height = image.size
                if make_copy:
                        image = image.copy()
                raw = image.load()
                return image, raw, width, height

        @classmethod
        def __format_ret(cls, image):
                return image.convert("1")

        @classmethod
        def rand(cls, image):
                image, raw, width, height = cls.__prepare(image)
                for y in range(height):
                        for x in range(width):
                                raw[x, y] = 0 if raw[x, y] < rand.uniform(0,255) else 255
                return cls.__format_ret(image)

        @classmethod
        def __avg_color(cls, image):
                image, raw, width, height = cls.__prepare(image, make_copy=False)
                sum = 0
                for y in range(height):
                        for x in range(width):
                                sum += raw[x, y]
                return sum / (width * height)

        @classmethod
        def __color_hist(cls, image):
                image, raw, width, height = cls.__prepare(image, make_copy=False)
                stats = [0]*256
                for y in range(height):
                        for x in range(width):
                                stats[raw[x, y]] += 1
                return stats

        @classmethod
        def __choose_optimal_lvl(cls, image):
                color_hist = cls.__color_hist(image)
                avg_color = cls.__avg_color(image)
                total = sum(color_hist)
                white = total
                lvl = 0
                while white*255/total > avg_color:
                        white -= color_hist[lvl]
                        lvl += 1
                return lvl

        @classmethod
        def threshold(cls, image):
                image, raw, width, height = cls.__prepare(image)
                lvl = cls.__choose_optimal_lvl(image)
                for y in range(height):
                        for x in range(width):
                                raw[x, y] = 0 if raw[x,y] < lvl else 255
                return cls.__format_ret(image)

        @classmethod
        def byline(cls, image):
                image, raw, width, height = cls.__prepare(image)
                diff = 0
                for y in range(height):
                        for x in range(width):
                                curr_diff =  raw[x, y] + diff
                                raw[x, y] = 0 if curr_diff < 128 else 255
                                diff = curr_diff - raw[x, y]
                return cls.__format_ret(image)

        @classmethod
        def fstein(cls, image):
                image, raw, width, height = cls.__prepare(image)
                diff = np.zeros((width, height))
                for y in range(height):
                        x_vals = range(width)
                        pos = 1
                        if y % 2 != 0:
                                x_vals = reversed(x_vals)
                                pos = -pos
                        for x in x_vals:
                                curr_diff = raw[x, y] + diff[x, y]
                                raw[x, y] = 0 if curr_diff < 128 else 255
                                curr_diff -= raw[x, y]
                                def add_safe(x, y, val):
                                        if 0 <= x < width and 0 <= y < height:
                                                diff[x, y] += val
                                add_safe(x + pos, y, .4375*curr_diff)
                                add_safe(x + pos, y + 1, .0625*curr_diff)
                                add_safe(x, y + 1, .3125*curr_diff)
                                add_safe(x - pos, y + 1, .1875*curr_diff)
                return cls.__format_ret(image)

        @classmethod
        def __bayer_matrix(cls, r):
                if r == 2:
                        return np.array([[0, 2], [3, 1]])
                matrix_part = cls.__bayer_matrix(r//2)
                matrix = np.empty((r, r), dtype=int)
                matrix[:r//2, :r//2] = 4*matrix_part
                matrix[:r//2, r//2:] = 4*matrix_part + 2
                matrix[r//2:, :r//2] = 4*matrix_part + 3
                matrix[r//2:, r//2:] = 4*matrix_part + 1
                return matrix

        @classmethod
        def ordered(cls, image, r = 4):
                image, raw, width, height = cls.__prepare(image)
                lvls = cls.__bayer_matrix(r) * 256 / (r * r)
                for y in range(height):
                        for x in range(width):
                                raw[x, y] = 0 if raw[x, y] < lvls[x % r, y % r] else 255
                return cls.__format_ret(image)


def get_data():
        methods = {
                'ordered': Dithering.ordered,
                'fstein': Dithering.fstein,
                'byline': Dithering.byline,
                'threshold': Dithering.threshold,
                'rand': Dithering.rand,
        }
        parser = argparse.ArgumentParser(description='Dithering effects')
        parser._action_groups.pop()
        required = parser.add_argument_group('required arguments')
        optional = parser.add_argument_group('optional arguments')
        required.add_argument('-f', '--image', required=True, nargs=1, help='Image to process')
        required.add_argument('-m', '--method', required=True, nargs=1, default='ordered',
                              help='available methods: {}'.
                              format(str.join(', ', methods.keys())))
        optional.add_argument('-o', '--output', default=None, help='Output image name')
        args = parser.parse_args()
        method = args.method[0]
        filename = args.image[0]
        outfilename = args.output or method+"_"+filename
        if method not in methods.keys():
                print("Invalid method '{}'. Available methods: {}.".
                        format(method, str.join(', ', methods.keys())))
                sys.exit(1)
        try:
                image = Image.open(filename)
        except IOError as io:
                print("Can't opent file '{}': {}".format(filename, io))
                sys.exit(1)
        return image, methods[method], outfilename

def main():
        image, func, outfilename = get_data()
        new_image = func(image)
        try:
                new_image.save(outfilename)
        except IOError as io:
                print("Can't save file '{}': {}".format(outfilename, io))
                sys.exit(1)

if __name__ == "__main__":
        main()
