# ImageDithering

Dither is an intentionally applied form of noise used to randomize quantization error, preventing large-scale patterns such as color banding in images. Dither is routinely used in processing of both digital audio and video data, and is often one of the last stages of mastering audio to a CD.

This program implements different Dithering filers - `Thresholding`, `Random dithering`, `Ordered dithering`, `Floyd–Steinberg`, `ByLine`

## Usage:
```
# python main.py
Usage: main.py [-h] -f IMAGE -m METHOD [-o OUTPUT]
main.py: error: the following arguments are required: -f/--image, -m/--method
```
Example:
```
# python main.py --image samples/Lenna.pgm --method fstein  --output=lena_fstein.pgm
```
Methods: `ordered`, `fstein`, `byline`, `threshold`, `rand`
